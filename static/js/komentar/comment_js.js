$(document).ready(function() {
    var article_id = id_artikel;

    // memanggil object
    $.ajax({
        url: "/data_comment?q=" + article_id,
        success: function(data) {
            var items = data.items;
            var totalItems = data.totalItems;

            $('#jumlah_komentar').html(totalItems);

            for (i = 0; i < data.totalItems; i++) {
                var commenter = items[i].commenter;
                var comment = items[i].comment;
                var waktu = items[i].waktu;

                $('#comment_list').append(`<div class='container'><h4>${commenter}</h4><h5>${comment}</h5><p>${waktu}</p></div>`)
            }
        }
    });

    // membuat object
    $('#komentar_form').on('submit', function() {
        if ( $.trim($("#comment").val()) === "" ) {
            alert('don\'t leave the field empty')
            return false
        }
        $.ajax({
            type: "POST",
            url: url_submit,
            data: {
                'comment': $('#comment').val(),
                'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()
            },
        });
    });

    // lain-lain
    $('#comment').focus(function() {
        $('#submit_button').show();
        $('#cancel_button').show();

        $(this).keyup(function() {
            if ( $.trim( $(this).val() ) ) {
                $('#submit_button').prop("disabled", false);
            }
            else {
                $('#submit_button').prop("disabled", true);
            };
        })
        
    });
    $('#cancel_button').click(function() {
        $('#comment').val('');
        $('#submit_button').prop("disabled", true);
        $('#submit_button').hide();
        $('#cancel_button').hide();
    });
});