from django.test import TestCase, Client
from django.contrib.auth.models import User
from .models import *
import json, time

# Create your tests here.
class TestKomentar(TestCase):
    def setUp(self):
        # membuat user
        self.client = Client()
        self.credentials = {
            'username': 'test_user',
            'password': 'test_password'
        }
        self.user = User.objects.create_user(**self.credentials)

        self.article = Article(artikel='test_artikel', penulis='test_penulis', deskripsi='test_deskripsi')
        self.article.save()

    def test_komentar_tidak_dapat_dibuat_belum_login(self):
        komentar = self.client.post('/add_comment/1/', {'comment':'test_comment'})
        response = self.client.get('/article/1/')

        self.assertEqual(komentar.status_code, 302)
        self.assertEqual(self.article.komentar_set.all().count(), 0)


    def test_komentar_dapat_dibuat_sudah_login(self):
        # login
        self.client.login(**self.credentials)

        # post komentar
        self.client.post('/add_comment/1/', {'comment':'test_comment'})

        # cek jika berhasil
        self.assertEqual(self.article.komentar_set.all().count(), 1)

    def test_komentar_json(self):
        response = Client().get('/data_comment', data={'q':self.article.id})
        self.assertEqual(response.status_code, 200)

        # no comments added yet
        self.assertEqual(json.loads(response.content)["totalItems"], 0)

        # with added comments
        ## login
        self.client.login(**self.credentials)
        ## leave comment
        self.client.post('/add_comment/1/', {'comment':'test_comment'})
        ## read jsonresponse
        response = Client().get('/data_comment', data={'q':self.article.id})
        self.assertEqual(json.loads(response.content)["totalItems"], 1)

    def test_data_komentar_json_sesuai_dengan_django_queryset(self):
        # login
        self.client.login(**self.credentials)
        # leave comments
        self.client.post('/add_comment/1/', {'comment':'test_comment 1'})
        time.sleep(1)
        self.client.post('/add_comment/1/', {'comment':'test_comment 2'})

        response = Client().get('/data_comment', data={'q':self.article.id})
        # Memeriksa bahwa jumlah komentar pada jsonresponse dan pada django models ORM sama
        self.assertEqual(self.article.komentar_set.all().count(), json.loads(response.content)["totalItems"])
        # memeriksa bahwa urutan sesuai (komentar terakhir muncul terlebih dahulu)
        waktu_komentar_yang_muncul_pertama = json.loads(response.content)["items"][0]["waktu"]
        waktu_komentar_yang_muncul_kedua = json.loads(response.content)["items"][1]["waktu"]

        self.assertTrue(waktu_komentar_yang_muncul_pertama > waktu_komentar_yang_muncul_kedua)