from django.urls import path
from . import views

app_name = 'komentar'
urlpatterns = [
    path('add_comment/<int:id>/', views.add_comment, name='add_comment'),
    path('data_comment', views.data_comment, name='data_comment'),
]
