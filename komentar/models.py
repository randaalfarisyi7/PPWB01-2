from django.db import models
from django.utils import timezone
from article.models import Article
from django.contrib.auth.models import User

# Create your models here.
class Komentar(models.Model):
    commenter = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    comment = models.TextField()
    waktu = models.DateTimeField(default=timezone.now)
    artikel = models.ForeignKey(Article, on_delete=models.CASCADE, null=True)