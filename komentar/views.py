from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import *
from article.models import Article
from django.http import JsonResponse
from django.utils import dateformat, timezone
from django.utils.html import strip_tags

# Create your views here.
@login_required
def add_comment(request, id):
    artikel = get_object_or_404(Article, id=id)
    data = {'success': False}
    comment = Komentar()
    if request.method == 'POST':
        comment.artikel = artikel
        comment.commenter = request.user
        comment.comment = strip_tags(request.POST['comment'])
        comment.save()
        data['success'] = True
    return JsonResponse(data)

def data_comment(request):
    # Mengambil object artikel dangan id pada argumen q
    artikel_id = request.GET['q']
    artikel = Article.objects.get(id=artikel_id)
    query_comment_list = artikel.komentar_set.all().order_by('-waktu')

    # Mengubah array kedalam bentuk dictionary untuk json response
    comment_list={
        "totalItems":len(query_comment_list),
        "items"     :[]
    }

    for comment in query_comment_list:
        formatted_date = dateformat.format(timezone.localtime(comment.waktu), 'd-m-Y H:i:s')
        dict_of_comment = {
            'commenter' : comment.commenter.username,
            'comment'   : comment.comment,
            'waktu'     : formatted_date
        }
        comment_list['items'].append(dict_of_comment)
    
    return JsonResponse(comment_list)

