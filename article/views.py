from django.shortcuts import render, get_object_or_404, redirect
from .models import Article
from .forms import ArticleForm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

# Create your views here.
@login_required(login_url='/login')
def article_list(request):
    artikels = Article.objects.all()
    return render(request, 'article/landing.html', {'artikels':artikels})

def article_page(request, id):
    artikel = get_object_or_404(Article, id=id)

    response = {
        'artikel':artikel,
    }
    
    return render(request, 'article/view.html', response)

def article_add(request):
    data = {'success': False}
    artikels = Article()
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            artikel = form.save()
            artikel.save()
            return redirect('article:article_page', id=artikel.id)
    else:
        form = ArticleForm()
        artikel = Article.objects.all()
        context = {'form':form,'artikel':artikel}
        return render(request, 'article/form.html', context)

def article_delete(request, id):
    Article.objects.get(id=id).delete()
    return redirect('article:article_list')

def data_article(request):
    id_artikel = request.GET['q']
    artikel = Article.objects.get(id=id_artikel)

    dict_artikel = {
        'judul': artikel.artikel,
        'penulis': artikel.penulis,
        'deskripsi': artikel.deskripsi,
        'tanggal_penulisan': artikel.tanggal_penulisan
    }

    return JsonResponse(dict_artikel)

