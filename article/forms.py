from django import forms
from .models import Article

class ArticleForm(forms.ModelForm):
    class Meta:
        # artikel = forms.CharField(label="Nama Artikel",max_length=50,widget=forms.TextInput(attrs={"placeholder":"","class":"form-control",}))
        # penulis = forms.CharField(label="Penulis Artikel",max_length=50,widget=forms.TextInput(attrs={"placeholder":"","class":"form-control",}))
        # deskripsi = forms.CharField(label="Deskripsi",widget=forms.TextInput(attrs={"placeholder":"Tulis Konten Artikel Anda","class":"form-control",}))
        model = Article
        fields = ['artikel', 'penulis', 'deskripsi']
        labels = {
            'artikel':'Nama Artikel',
            'penulis':'Penulis Artikel',
            'deskripsi':'Deskripsi',
        }
        widgets = {
            'artikel':forms.TextInput(attrs={
                'placeholder':'',
                'class':'form-control',
            }),
            'penulis': forms.TextInput(attrs = {
                'placeholder':'',
                'class':'form-control',
            }),
            'deskripsi':forms.TextInput(attrs={
                'placeholder':'Tulis Konten Artikel Anda Disini',
                'class':'form-control',
            }),
        
        }