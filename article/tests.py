
from django.test import TestCase,Client
from django.contrib.auth.models import User
from django.urls import resolve
from .views import article_list,article_add,article_page,article_delete
from .models import Article
from .forms import *
import json, time

class TestingArtikel(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john','lennon@thebeatles.com','johnpassword')

    def test_if_article_model_exist(self):
        self.client.login(username='john', password='johnpassword')
        self.client.post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        hitung_jumlah_data = Article.objects.all().count()
        self.assertEquals(hitung_jumlah_data, 1)
    
    def test_if_article_form_is_valid(self):
        articleForm = ArticleForm(data={'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        self.assertTrue(articleForm.is_valid())
    
    def test_if_article_form_is_invalid(self):
        articleForm = ArticleForm(data={})
        self.assertFalse(articleForm.is_valid())
    
    def test_main_url_is_exist (self) :
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/article_list/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/add/')
        self.assertEqual(response.status_code, 200)
    
    def test_article_page_url_is_exist(self):
        self.client.login(username='john', password='johnpassword')
        contohArtikel = self.client.post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        response = self.client.get('/article/1/', {}, True)
        self.assertEquals(response.status_code, 200)
    
    def test_if_article_list_template_exists_on_the_page(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/article_list/')
        self.assertTemplateUsed(response, 'article/landing.html')
    
    def test_if_article_add_template_exists_on_the_page(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/add/')
        self.assertTemplateUsed(response, 'article/form.html')
    
    def test_if_article_page_template_exists_on_the_page(self):
        self.client.login(username='john', password='johnpassword')
        self.client.post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        response = self.client.get('/article/1/',{},True)
        self.assertTemplateUsed(response, 'article/view.html')

    def test_article_json(self):
        contohArtikel = self.client.post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        response = self.client.get('/article/1/', {}, True)
        self.assertEqual(response.status_code, 200)
        self.client.login(username='john', password='johnpassword')
        self.client.post('/add/', {'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})



