from django.db import models
from django.utils import timezone

# Create your models here.
class Article(models.Model):
    artikel = models.CharField(max_length=50, help_text='Nama Artikel')
    penulis = models.CharField(max_length=50, help_text='Nama Penulis')
    deskripsi = models.TextField(help_text='Deskripsi')
    tanggal_penulisan = models.DateTimeField(default=timezone.now,help_text='Tanggal Penulisan')
    def __str__(self):
        return "{} | {} | {} | {}".format(self.artikel, self.penulis, self.deskripsi, self.tanggal_penulisan)

