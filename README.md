# Tugas Kelompok PPW B01
![Pipeline](https://gitlab.com/randaalfarisyi7/PPWB01-2/badges/master/pipeline.svg) ![coverage report](https://gitlab.com/randaalfarisyi7/PPWB01-2/badges/master/coverage.svg)

## Anggota Kelompok:
1. Randa Alfarisyi - 1606877250
2. Muhammad Azhar Hassanuddin - 1806235763
3. Muhammad Ali Asadillah - 1806186660


## Link Heroku:
https://cov19b01.herokuapp.com/

