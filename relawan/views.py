from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
import requests
import json
from .forms import Input_Form
from .models import RelawanList
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.forms.models import model_to_dict

# Create your views here.
@login_required(redirect_field_name='next', login_url='/login')
def relawan(request):
	relawans = RelawanList.objects.all()
	response = {'relawans' : relawans, 'input_form' : Input_Form}
	return render(request, 'main/relawan.html', response)

@login_required(login_url='/login')
def formRelawan(request):
	response = 	{'input_form' : Input_Form}
	return render(request, 'main/formRelawan.html', response)

def saverelawan(request):
	form = Input_Form()

	if request.is_ajax():
		form = Input_Form(request.POST)
		if form.is_valid():
				form.save()
				return JsonResponse({
					'message':'success'
					})
	return render(request,'main/relawan.html', {'form',form})

def dataKasus(request):
	relawans = RelawanList.objects.all().values()
	data = list(relawans)
	return JsonResponse(data, safe=False)

"""def newRelawan(request):
	form = Input_Form(request.POST)
	if request.method == 'POST':
		if form.is_valid():
				form.save()
				return HttpResponseRedirect('/relawan')
		else:
				return HttpResponseRedirect('/formrelawan')"""
