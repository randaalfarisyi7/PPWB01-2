from django import forms
from .models import RelawanList

class Input_Form(forms.ModelForm):
	class Meta:
		model = RelawanList
		fields = ['display_name','alamat','donasi']
		error_messages = {
			'required' : 'Please Type'
		}
	input_attrs = {
		'type' : 'text',
		'placeholder' : 'Nama'
	}
	alamat_input = {
		'type' : 'text',
		'placeholder' : 'Alamat'
	}
	donasi_input = {
		'type' : 'text',
		'placeholder' : 'Jumlah Donasi'
	}

	display_name = forms.CharField(label='', required=True, max_length=60, widget=forms.TextInput(attrs=input_attrs))
	alamat = forms.CharField(label='', required=True, max_length=200, widget=forms.TextInput(attrs=alamat_input))
	donasi = forms.CharField(label='', required=True, max_length=40, widget=forms.TextInput(attrs=donasi_input))


